import React from 'react';
import { Router, Switch } from 'react-router-dom';
import { createBrowserHistory as createHistory } from 'history';
import EditorRoutes from './EditorRoutes';
//import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import Main from '../Components/Layouts/Pages/Main';
import EditorLogin from '../Components/Layouts/Pages/EditorLogin';
import EditorReg from '../Components/Layouts/Pages/EditorReg';
import NotFoundPage from '../Components/Layouts/Pages/NotFoundPage';
import EditorDasboard from '../Components/Dashboard/Editor/Dashboard';
import Portfolio from '../Components/Dashboard/Editor/Portfolio';
import OurTeam from '../Components/Dashboard/Editor/OurTeam';
import News from '../Components/Dashboard/Editor/News';

export const history = createHistory();
class AppRouter extends React.Component {

  render() {
    return (
      <Router history={history} >
        <div>
          <Switch>
            <PublicRoute path="/" component={Main} exact={true} />
            <PublicRoute path="/editor-login" component={EditorLogin} exact={true} />
            <PublicRoute path="/editor-reg" component={EditorReg} exact={true} />
            <EditorRoutes path="/editor-dasboard" component={EditorDasboard} exact={true} />
            <EditorRoutes path="/editor-portfolio" component={Portfolio} exact={true} />
            <EditorRoutes path="/editor-ourteam" component={OurTeam} exact={true} />     
            <EditorRoutes path="/editor-news" component={News} exact={true} />     

            <PublicRoute component={NotFoundPage} />
          </Switch>
        </div>
      </Router>
    )
  }
};
export default AppRouter;
