import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink, MDBRow, MDBCol } from "mdbreact";
import about from '../../../images/about.png';
import mission from '../../../images/mission.jpg';
import values from '../../../images/values.png';

class TabsDefault extends Component {

    state = {
        activeItem: "1",

    };


    toggle = tab => e => {
        if (this.state.activeItem !== tab) {
            this.setState({
                activeItem: tab
            });
        }
    };

    render() {
        return (
            <Router>
                <MDBContainer id='about'>
                    <MDBRow><h3 style={{ margin: 'auto', marginTop: '3%' }}>About Us</h3></MDBRow>
                    <MDBRow>
                        <MDBCol
                            md="2"
                            className="text-md-left mt-xl-5 mb-5"
                        >
                            <MDBNav className="flex-column">
                                <MDBNavItem>
                                    <MDBNavLink disabled to="#!" style={{ backgroundColor: '#00982d', color: 'white', textAlign: 'center' }}>About Us</MDBNavLink>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink to="#" active={this.state.activeItem === "1"} onClick={this.toggle("1")} role="tab" className="flex-column-tab" >
                                        Who We are</MDBNavLink>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink to="#" active={this.state.activeItem === "2"} onClick={this.toggle("2")} role="tab" className="flex-column-tab" >
                                        Our Mission</MDBNavLink>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink to="#" active={this.state.activeItem === "3"} onClick={this.toggle("3")} role="tab" className="flex-column-tab" >
                                        Our Values</MDBNavLink>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink to="#" active={this.state.activeItem === "4"} onClick={this.toggle("4")} role="tab" className="flex-column-tab" >
                                        Our Team</MDBNavLink>
                                </MDBNavItem>
                            </MDBNav>
                        </MDBCol>
                        <MDBCol md="10" className=" mt-xl-5 mb-5" >


                            <MDBTabContent activeItem={this.state.activeItem} >

                                <MDBTabPane tabId="1" role="tabpanel">

                                    <div style={{ width: '200px', marginLeft: '12%', marginRight: '4%', float: 'left' }}>
                                        <img alt='' src={about} style={{ width: '100%' }} />
                                    </div>

                                    <p className="mt-2" >
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                        Nihil odit magnam minima, soluta doloribus reiciendis
                                        molestiae placeat unde eos molestias. Quisquam aperiam,
                                        pariatur. Tempora, placeat ratione porro voluptate odit
                                            minima. </p>

                                </MDBTabPane>

                                <MDBTabPane tabId="2" role="tabpanel">
                                    <div style={{ width: '200px', marginLeft: '12%', marginRight: '4%', float: 'left' }}>
                                        <img alt='' src={mission} style={{ width: '100%' }} />
                                    </div>

                                    <p className="mt-2">
                                        Quisquam aperiam, pariatur. Tempora, placeat ratione porro
                                        voluptate odit minima. Lorem ipsum dolor sit amet,
                                        consectetur adipisicing elit.</p>
                                    <p>
                                        Quisquam aperiam, pariatur. Tempora, placeat ratione porro
                                        voluptate odit minimas.</p>
                                </MDBTabPane>
                                <MDBTabPane tabId="3" role="tabpanel">
                                    <div style={{ width: '200px', marginLeft: '12%', marginRight: '4%', float: 'left' }}>
                                        <img alt='' src={values} style={{ width: '100%' }} />
                                    </div>

                                    <p className="mt-2">
                                        Quisquam aperiam, pariatur. Tempora, placeat ratione porro
                                        voluptate odit minima. Lorem ipsum dolor sit amet,
                                        consectetur adipisicing elit. Nihil odit magnam minima,
                                        soluta doloribus reiciendis molestiae placeat unde eos
                                        molestias.</p>
                                </MDBTabPane>
                                <MDBTabPane tabId="4" role="tabpanel">
                                    
                                </MDBTabPane>
                            </MDBTabContent>
                        </MDBCol>
                    </MDBRow>


                </MDBContainer>
            </Router>
        );
    }
}
export default TabsDefault;