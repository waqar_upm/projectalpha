import React, { useState } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon, MDBInput, MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';


import Upload from './Upload';
import './editor.css';
const Portfolio = () => {
    const [sector, setSector] = useState('');
    return (
        <MDBContainer>
            <p className="h4 text-center mb-4">Portfolio</p>
            <MDBRow>
                <MDBCol md="6">
                    <form>
                        
                        <MDBRow>
                            <MDBCol md="12">
                                <MDBInput
                                    label="Details"
                                    type="text"
                                />
                            </MDBCol>
                        </MDBRow>

                        <MDBRow>
                            <MDBCol md="12">
                                <FormControl style={{ width: '100%', textAlign: 'left' }}>
                                    <InputLabel htmlFor="Sector" style={{ width: '100%', textAlign: 'left' }}>Sector</InputLabel>
                                    <Select
                                        style={{ width: '100%', textAlign: 'left' }}
                                        value={sector}
                                        onChange={(e) => {
                                            setSector(e.target.value);
                                        }}
                                        inputProps={{
                                            name: 'Sector',
                                            id: 'Sector',
                                        }}
                                    >
                                        <MenuItem value='A'>Sector A</MenuItem>
                                        <MenuItem value='B'>Sector B</MenuItem>
                                        <MenuItem value='C'>Sector C</MenuItem>
                                    </Select>
                                </FormControl>
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>
                            <MDBCol md="12">
                                <Upload />
                            </MDBCol>
                        </MDBRow>

                        <div className="text-center mt-4">
                            <MDBBtn color="info" outline type="submit">
                                Save <MDBIcon far icon="paper-plane" className="ml-2" />
                            </MDBBtn>
                        </div>
                    </form>
                </MDBCol>
                <MDBCol md="6">
                    <MDBTable style={{marginTop: '25px',}}>
                        <MDBTableHead >
                            <tr>
                                <th>Image</th>
                                <th>Details</th>
                                <th>Sector</th>
                                <th>Delete</th>
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>
                            <tr>
                                <td>1</td>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td><DeleteForeverIcon /></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td><DeleteForeverIcon /></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td><DeleteForeverIcon /></td>
                            </tr>
                        </MDBTableBody>
                    </MDBTable>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

export default Portfolio;