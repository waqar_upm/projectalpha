import React, { useState } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon, MDBInput, MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import Upload from './Upload';
import Rating from '@material-ui/lab/Rating';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';


const OurTeam = () => {
    const [rating, setRating] = useState(0);
    return (
        <MDBContainer>
            <p className="h4 text-center mb-4">Team</p>
            <MDBRow>
                <MDBCol md="6">
                    <form>
                        
                        <MDBRow>
                            <MDBCol md="12">
                                <MDBInput
                                    label="Name"
                                    type="text"
                                />
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>
                            <MDBCol md="12">
                                <Rating name='rating' value={rating} precision={0.5} onChange={(e) => {
                                    setRating(parseFloat(e.target.value));
                                }} />
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>
                            <MDBCol md="12">
                                <MDBInput type="textarea" label="Description" rows="3" />
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>
                            <MDBCol md="12">
                                <Upload />
                            </MDBCol>
                        </MDBRow>
                        <div className="text-center mt-4">
                            <MDBBtn color="info" outline type="submit">
                                Save <MDBIcon far icon="paper-plane" className="ml-2" />
                            </MDBBtn>
                        </div>
                    </form>
                </MDBCol>
                <MDBCol md="6">
                    <MDBTable style={{marginTop: '25px',}}>
                        <MDBTableHead >
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Rating</th>
                                <th>Description</th>
                                <th>Delete</th>
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>
                            <tr>
                                <td>1</td>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                <td><DeleteForeverIcon /></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                                <td><DeleteForeverIcon /></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                                <td><DeleteForeverIcon /></td>
                            </tr>
                        </MDBTableBody>
                    </MDBTable>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

export default OurTeam;