const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var cors = require('cors');
const passport = require('passport');
require('./passport')(passport);

const app = express();
app.use(cors());

const options = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  autoIndex: false, // Don't build indexes
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
  family: 4 // Use IPv4, skip trying IPv6
};

mongoose.connect('mongodb+srv://mongo:mongo123@cluster0-0wltj.gcp.mongodb.net/testing?retryWrites=true&w=majority',options)
.then(() => {
  console.log('Connected to Database!');
})
.catch((err) => {
  console.log('Connection Failed!', err);
});



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS");
  next();
});
app.use('/auth', require('./router/auth'));
app.use('/user', require('./router/users'));
app.use('/basic', require('./router/basic'));
app.use('/news', require('./router/news'));

module.exports = app;
