const app = require('express').Router();
const Basic = require('../models/BasicInfo');
app.get('/', (req, res, next) => {
    Basic.findOne()
        .then(documents => {
            res.status(200).json({
                message: "Success",
                info: documents
            });
        });
});

app.post('/', (req, res, next) => {
    const { id, ...rest } = req.body;
    let basic = { _id: id, ...rest };
    if (id == '') {
        basic = new Basic({ ...rest });
    }
    Basic.updateOne({ _id: basic._id },
        basic,
        { upsert: true, setDefaultsOnInsert: true }).then(documents => {
            res.status(201).json({
                message: 'Information Updated Successfully!',
                info: documents
            });
        });
});
module.exports = app;