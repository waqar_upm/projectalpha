const mongoose = require('mongoose');

const News = mongoose.Schema({
  title: { type: String, require: true },
  body: { type: String, require: true },
  sector: { type: String, require: true },
  image: { type: String, require: true },
  updated: { type: Date, default: Date.now() },
});

module.exports = mongoose.model('News', News);
